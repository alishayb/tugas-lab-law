from fastapi import FastAPI, File, Form, UploadFile
import pymongo
from pydantic import BaseModel
from datetime import date
import os
import aiofiles as aiofiles

app = FastAPI()

class Plan(BaseModel):
    date:str = date.today().strftime("%d/%m/%Y")
    title:str = "Plan title"
    description:str = "Plan description"

plan_id = 1

client = pymongo.MongoClient("mongodb://alishayb:HzzdOIJ2Ug5MlTbE@lab-law-shard-00-00.qkcwy.mongodb.net:27017,lab-law-shard-00-01.qkcwy.mongodb.net:27017,lab-law-shard-00-02.qkcwy.mongodb.net:27017/planner_database?ssl=true&replicaSet=atlas-bt69dl-shard-0&authSource=admin&retryWrites=true&w=majority")
db = client['planner_database']
planner_db = db.planner_database

@app.get("/")
def planner():
    plans = []
    certain_date = {"date": date.today().strftime("%d/%m/%Y")}
    for plan in planner_db.find(certain_date):
        plans.append({
            'id': plan["id"],
            'date':plan["date"],
            'title':plan["title"],
            'description':plan["description"]
        })

    if plans == [] :
        return {'Today plans': "Hooray, there are no plans today!"}
    else:
        return {'Today plans': plans}

@app.get("/get-plan/{date}")
def get_plan(date:str):
    date = date.replace('-','/');
    plans = []
    certain_date = {"date": date}
    for plan in planner_db.find(certain_date):
        plans.append({
            'id': plan["id"],
            'date':plan["date"],
            'title':plan["title"],
            'description':plan["description"]
        })

    message = "Hooray! There are no plans at " + date
    if plans == [] :
        return {"Your plans": message}
    else:
        return {'Your plans': plans}

@app.post("/create-plan")
def create_plan(plan:Plan):
    global plan_id

    date = plan.date
    title = plan.title
    description = plan.description

    plan = {
        'id': plan_id,
        'date'  : date,
        'title' : title,
        'description' : description
    }

    plan_id += 1

    planner_db.insert_one(plan)
    message = f'Successfully creating plan: {title} to be set on {date}';
    return {'Message': message} 

@app.delete("/delete-plan/{plan_id}")
def delete_plan(plan_id: int):
    deleted_plan = planner_db.find_one({'id': plan_id})
    planner_db.delete_one({'id': plan_id})

    message = 'Successfully deleting plan: ' + deleted_plan['title']
    return {'Message': message}

@app.put("/update-plan/{plan_id}")
def update_plan(plan_id:int, new_data:Plan):
    old_plan = planner_db.find_one({'id': plan_id})

    date = new_data.date
    title = new_data.title
    description = new_data.description

    updated_plan = {
        'date'  : date,
        'title' : title,
        'description' : description
    }

    planner_db.update_one({'id': plan_id}, {'$set' : updated_plan})
    message = 'Successfully updating plan: ' + updated_plan['title']
    return {'Message': message}

@app.post("/files/")
async def create_file(plan_file: UploadFile = File(...)):
    filename = plan_file.filename
    file_path = './files/' + filename
    with open(file_path, 'wb') as file:
        content = await plan_file.read()
        file.write(content)
        file.close()
    message = 'Successfully saved ' + filename
    return {'Message': message}

