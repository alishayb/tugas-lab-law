from fastapi import FastAPI
from pydantic import BaseModel
import logging
from logstash_async.handler import AsynchronousLogstashHandler
from logstash_async.handler import LogstashFormatter

app = FastAPI()

class User(BaseModel):
    name:str = ''
    birthday:str = ''

user = User()

# Create the logger and set it's logging level
logger = logging.getLogger("logstash")
logger.setLevel(logging.DEBUG)        

# Create the handler
handler = AsynchronousLogstashHandler(
    host='3ee0f3c1-81df-45b4-ac93-abfe0f12620b-ls.logit.io', 
    port= 10004, 
    ssl_enable=False, 
    ssl_verify=False,
    database_path='')

# Here you can specify additional formatting on your log record/message
formatter = LogstashFormatter()
handler.setFormatter(formatter)

# # Assign handler to the logger
logger.addHandler(handler)

@app.get("/")
def planner():
    return {
        'Name': user.name,
        'Birthday': user.birthday,
    }

@app.post("/register")
def register(user_inp:User):
    logger.debug('Inputted name: ' + user_inp.name)
    logger.debug('Inputted birthday: ' + user_inp.birthday)
    
    bday = user_inp.birthday.split('-')

    try:
        date = int(bday[0])
        month = int(bday[1])
        year = int(bday[2])

        user.name = user_inp.name
        user.birthday = user_inp.birthday

        return {
            'Status': 'Registration success',
            'Name': user.name,
            'Birth date': date,
            'Birth month': month,
            'Birth year': year
        }

    except Exception as e:
        logger.error(e)
        return {'Status': 'Registration failed'}
