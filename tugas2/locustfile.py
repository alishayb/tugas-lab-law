from locust import HttpUser, task, between
import random

class PlannerUser(HttpUser):
    wait_time = between(3, 10)

    @task(5)
    def get_today_plan(self):
        self.client.get("/")
    
    @task(3)
    def get_certain_date_plan(self):
        year = random.randint(2010, 2030)
        month = random.randint(1,12)
        date = random.randint(1,28)
        if month < 10:
            month = '0' + f"{month}"
        if date < 10:
            date = '0' + f"{date}"
        
        certain_date = f"{year}-{month}-{date}"
        
        self.client.get(f"/get-plan/{certain_date}")
    
    @task    
    def update_plan(self):
        updated = {
            "date": "2022-03-21",
            "title": "Endurance LAW",
            "description": "Melakukan endurance test LAW"
        }
        self.client.post("/update-plan/2", json=updated)