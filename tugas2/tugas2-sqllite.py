from fastapi import FastAPI
from pydantic import BaseModel
from datetime import date
import sqlite3 as sl

app = FastAPI()

class Plan(BaseModel):
    date:str = date.today().strftime("%Y-%m-%d")
    title:str = "Plan title"
    description:str = "Plan description"

con = sl.connect('planner.db', check_same_thread=False)
with con:
    con.execute("""
        CREATE TABLE IF NOT EXISTS PLANNER (
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            date DATE NOT NULL,
            title TEXT NOT NULL,
            description TEXT NOT NULL
        );
    """)
    

@app.get("/")
def planner():
    plans = []
    certain_date = date.today().strftime("%Y-%m-%d")
    with con:
        
        # print(certain_date);
        plans_today = con.execute(f"SELECT * FROM PLANNER WHERE date='{certain_date}'")
        for plan in plans_today:
            plans.append({
                'id': plan[0],
                'date':plan[1],
                'title':plan[2],
                'description':plan[3]
            })

    if plans == [] :
        return {'Today plans': "Hooray, there are no plans today!"}
    else:
        return {'Today plans': plans}

@app.get("/get-plan/{certain_date}")
def get_plan(certain_date:str):
    plans = []
    with con:
        plans_at_certain_date = con.execute(f"SELECT * FROM PLANNER WHERE date='{certain_date}'")
        for plan in plans_at_certain_date:
            plans.append({
                'id': plan[0],
                'date':plan[1],
                'title':plan[2],
                'description':plan[3]
            })

    if plans == [] :
        return {'Today plans': f"Hooray, there are no plans at {certain_date}!"}
    else:
        return {'Today plans': plans}

@app.post("/update-plan/{plan_id}")
def update_plan(plan_id:int, new_data:Plan):
    date = new_data.date
    title = new_data.title
    description = new_data.description

    with con:
        con.execute(f"UPDATE PLANNER SET date='{date}', title='{title}', description='{description}' WHERE id={plan_id}")
    message = 'Successfully updating plan: ' + title
    return {'Message': message}

@app.post("/create-plan")
def create_plan(plan:Plan):
    date = plan.date
    title = plan.title
    description = plan.description
    
    with con:
        con.execute(f"INSERT INTO PLANNER (date, title, description) values('{date}', '{title}', '{description}')")
    
    message = f'Successfully creating plan: {title} to be set on {date}';
    return {'Message': message} 